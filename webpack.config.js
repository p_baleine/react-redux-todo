var webpack = require('webpack');
var path = require('path');

module.exports = {
  context: path.join(__dirname, './client'),
  output: {
    path: path.join(__dirname, './static'),
    filename: 'bundle.js',
  },
  entry: {
    tsx: './index.tsx',
    html: './index.html'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.ts', '.tsx', '.js']
  },
  module: {
    loaders: [
      { test: /\.html$/, loader: 'file?name=[name].[ext]' },
      { test: /\.tsx?$/, loader: 'babel-loader!ts-loader' },
      { test: /\.css$/, loaders: [
        'style-loader',
        'css-loader?modules'
        ]
      }
    ]
  },
  postcss: function () {
    return [require('autoprefixer'), require('precss')];
  }
};
