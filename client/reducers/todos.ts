import { handleActions, Action } from 'redux-actions';
import { v1 } from 'node-uuid';

import { Todo } from '../models/todos';
import {
   RECEIVE_TODOS, ADD_TODO, COMPLETE_TODO,
   TODO_POSTED
} from '../constants/action-types';

const todos = handleActions<Todo[]>({
  [RECEIVE_TODOS]: (state: Todo[], action: Action): Todo[] => {
    return action.payload.map(todo => Object.assign({}, todo, { key: v1() }));
  },

  [ADD_TODO]: (state: Todo[], action: Action): Todo[] => {
    return [...state, action.payload];
  },

  [COMPLETE_TODO]: (state: Todo[], action: Action): Todo[] => {
    return <Todo[]>state.map(todo =>
      todo.id === action.payload.id ?
        Object.assign({}, todo, action.payload) :
        todo
    );
  },

  [TODO_POSTED]: (state: Todo[], action: Action): Todo[] => {
    return <Todo[]>state.map(todo =>
      todo.key === action.payload.key ?
        Object.assign({}, todo, action.payload) :
        todo
    );
  }
});

export default todos;
