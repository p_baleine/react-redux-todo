import { createStore, applyMiddleware } from 'redux';
import * as thunk from 'redux-thunk';

import todos from './reducers/todos';

function configureStore(initialStore = []) {
  return applyMiddleware(
    <any>thunk
  )(createStore)(todos, initialStore);
}

export default configureStore;
