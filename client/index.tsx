import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './configure-store';
import App from './containers/app';

const store = configureStore();
const style = require('./style.css');

render(
  <Provider store={store}>
    <App className={style.todo} />
  </Provider>,
  document.getElementById('root')
);
