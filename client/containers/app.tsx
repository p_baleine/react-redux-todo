import * as React from 'react';
import { connect } from 'react-redux';
import { Component } from 'react';

import { fetchTodos, postTodo, updateTodo } from '../actions/todos';
import { Todo } from '../models/todos';

import TodoList from '../components/todo-list';
import AddTodo from '../components/add-todo';

class App extends Component<any, any> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchTodos());
  }

  render() {
    const { dispatch, todos } = this.props;
    return (
      <div className={this.props.className}>
        <AddTodo onAddTodo={text => {
          dispatch(postTodo(text))
        }} />
        <TodoList
          todos={todos}
          onTodoClick={todo => {
            dispatch(updateTodo(Object.assign({}, todo, { completed: !todo.completed })))
          }}
         />
      </div>
    );
  }
}

function todos(todos: Todo[]) {
  return {
    todos
  };
}

export default connect(todos)(App);
