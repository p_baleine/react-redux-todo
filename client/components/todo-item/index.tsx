import * as React from 'react';
import { Component } from 'react';

import { Todo } from '../../models/todos';

const style = require('./style.css');

export interface ITodoItemProps {
  text: string,
  completed: boolean,
  key: any,
  onClick: () => any
}

class TodoItem extends Component<ITodoItemProps, {}> {
  render() {
    return (
      <li className={`item ${style.item} ${this.props.completed ? style.completed : ''}`} >
        <label>
          <input
            type='checkbox'
            checked={this.props.completed}
            onClick={e => this.props.onClick()} />
          {this.props.text}
        </label>
      </li>
    );
  }
}

export default TodoItem;
