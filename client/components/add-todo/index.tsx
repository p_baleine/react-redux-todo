import * as React from 'react';
import { Component } from 'react';

export interface IAddTodoProps {
  onAddTodo: (string) => any
}

class AddTodo extends Component<IAddTodoProps, {}> {
  refs: {
    [string: string]: any;
    input: any;
  }

  render() {
    return (
      <form onSubmit={e => this.handleSubmit(e)}>
        <input type="text" ref="input"
               placeholder="What need to be done?" />
      </form>
    );
  }

  private handleSubmit(e) {
    e.preventDefault();
    var node = this.refs.input;
    var text = node.value.trim();
    this.props.onAddTodo(text);
    node.value = '';
  }
}

export default AddTodo;
