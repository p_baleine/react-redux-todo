import * as React from 'react';
import { Component } from 'react';

import { Todo } from '../../models/todos';
import TodoItem from '../todo-item';

export interface ITodoListProps {
  todos: Todo[],
  onTodoClick: (todo: Todo) => any
}

class TodoList extends Component<ITodoListProps, {}> {
  render() {
    return (
      <ul>
        {this.props.todos.map(todo =>
          <TodoItem
            key={todo.key}
            onClick={() => this.props.onTodoClick(todo)}
            {...todo}
           />
        )}
      </ul>    );
  }
}

export default TodoList;
