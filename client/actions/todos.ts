import { createAction } from 'redux-actions';
import { v1 } from 'node-uuid';

import { Todo } from '../models/todos';
import * as types from '../constants/action-types';

const receiveTodos = createAction<Todo[]>(
  types.RECEIVE_TODOS,
  (todos: Todo[]) => todos
);

const addTodo = createAction<Todo>(
  types.ADD_TODO,
  (todo: Todo) => todo
);

const completeTodo = createAction<Todo>(
  types.COMPLETE_TODO,
  (todo: Todo) => todo
)

const todoPosted = createAction<Todo>(
  types.TODO_POSTED,
  (todo: Todo, key) => Object.assign({}, todo, { key })
)

function fetchTodos() {
  var mock = [
    { id: 0, text: 'Hello, redux', completed: false }
  ];

  return (dispatch) => {
    return new Promise(resolve =>  resolve(mock))
    .then(todos => dispatch(receiveTodos(todos)));
  };
}

var id = 1;
function postTodo(text) {
  const key = v1();
  return (dispatch) => {
    dispatch(addTodo({ text: text, completed: false, key }));
    return new Promise(resolve => resolve({ text: text, completed: false, id: id++ }))
    .then(todo => dispatch(todoPosted(todo, key)));
  };
}

function updateTodo(todo: Todo) {
  return (dispatch) => {
    dispatch(completeTodo(todo));
    return new Promise(resolve => resolve(todo));
  }
}

export {
  fetchTodos,
  postTodo,
  updateTodo
};
