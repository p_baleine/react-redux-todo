export type Todo = {
  id?: number,
  text: string,
  completed: boolean,
  key?: any
};
